﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq.Expressions;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using CmlLib.Launcher;
using Newtonsoft.Json;

namespace Synchronizer
{
    public sealed class MinecraftInstance
    {
        private readonly string _host;
        private readonly int _port;

        public MinecraftInstance(string host, int port)
        {
            _host = host;
            _port = port;
            
            if (!Directory.Exists(".minecraft"))
                Directory.CreateDirectory(".minecraft");
        }


        public void SynchronizeFiles(ModpackEntry[] entries) => Synchronize(entries);

        public void Launch(int ram = 8192, string baseVersion = "1.12.2", string targetVersion = "forge-14.23.5.2808")
        {
            Minecraft.init(".minecraft");
            MLogin login = new MLogin();
            MSession session = null;
            session = login.TryAutoLogin();
            if (session == null || session.Result != MLoginResult.Success)
            {
                Console.WriteLine($"Autologin failure ({session?.Result}, {session?.ResultStr}), please reenter your credentials");
                Console.Write("Enter username: ");
                string username = Console.ReadLine();
                Console.Write("Enter password: ");
                string password = "";
                do
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    // Backspace Should Not Work
                    if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                    {
                        password += key.KeyChar;
                        Console.Write("*");
                    }
                    else
                    {
                        if (key.Key == ConsoleKey.Backspace && password.Length > 0)
                        {
                            password = password.Substring(0, (password.Length - 1));
                            Console.Write("\b \b");
                        }
                        else if (key.Key == ConsoleKey.Enter)
                        {
                            break;
                        }
                    }
                } while (true);
                Console.WriteLine("Logging in..");
                session = login.Authenticate(
                    username,
                    password
                );
                if (session.Result != MLoginResult.Success)
                    throw new Exception("Invalid credentials, probably, idk");
                else
                    Console.WriteLine("Logged in");
            }
            else
                Console.WriteLine("Logged in automatically");

            Console.WriteLine($"User {session.Username} has logged in");

            MProfileInfo[] infos = MProfileInfo.GetProfiles(); // TODO get profiles from local
            MProfileInfo baseInfo = null;
            MProfileInfo selInfo = null;
            foreach (MProfileInfo info in infos)
                if (info.Name == baseVersion)
                    baseInfo = info;
                else if (info.Name == targetVersion)
                    selInfo = info;
            MProfile profile = MProfile.Parse(baseInfo);
            MDownloader downloader = new MDownloader(profile);
            downloader.ChangeProgressEvent += e => Console.WriteLine($"Base download progress: {e.CurrentValue}/{e.MaxValue}");
            downloader.DownloadAll();
            profile = MProfile.Parse(selInfo);
            downloader = new MDownloader(profile);
            downloader.ChangeProgressEvent += e => Console.WriteLine($"Download progress: {e.CurrentValue}/{e.MaxValue}");
            downloader.DownloadAll();
            
            string executable = "java.exe";
            string directives =
                "-Dminecraft.applet.TargetDirectory=. -Dfml.ignorePatchDiscrepancies=true -Dfml.ignoreInvalidMinecraftCertificates=true -Duser.language=en -Duser.country=US -Djava.library.path=versions/forge-14.23.5.2808/forge-14.23.5.2808-natives-2637504945043 -Dminecraft.launcher.brand=java-minecraft-launcher -Dminecraft.launcher.version=1.6.91 -Dminecraft.client.jar=versions/1.12.2/1.12.2.jar";
            string java_arguments =
                $"-Xmx{ram}m -Xms256m -XX:PermSize=256m -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump";
            string libraries = "-cp libraries/net/minecraftforge/forge/1.12.2-14.23.5.2808/forge-1.12.2-14.23.5.2808.jar;libraries/net/minecraft/launchwrapper/1.12/launchwrapper-1.12.jar;libraries/org/ow2/asm/asm-all/5.2/asm-all-5.2.jar;libraries/org/jline/jline/3.5.1/jline-3.5.1.jar;libraries/net/java/dev/jna/jna/4.4.0/jna-4.4.0.jar;libraries/com/typesafe/akka/akka-actor_2.11/2.3.3/akka-actor_2.11-2.3.3.jar;libraries/com/typesafe/config/1.2.1/config-1.2.1.jar;libraries/org/scala-lang/scala-actors-migration_2.11/1.1.0/scala-actors-migration_2.11-1.1.0.jar;libraries/org/scala-lang/scala-compiler/2.11.1/scala-compiler-2.11.1.jar;libraries/org/scala-lang/plugins/scala-continuations-library_2.11/1.0.2/scala-continuations-library_2.11-1.0.2.jar;libraries/org/scala-lang/plugins/scala-continuations-plugin_2.11.1/1.0.2/scala-continuations-plugin_2.11.1-1.0.2.jar;libraries/org/scala-lang/scala-library/2.11.1/scala-library-2.11.1.jar;libraries/org/scala-lang/scala-parser-combinators_2.11/1.0.1/scala-parser-combinators_2.11-1.0.1.jar;libraries/org/scala-lang/scala-reflect/2.11.1/scala-reflect-2.11.1.jar;libraries/org/scala-lang/scala-swing_2.11/1.0.1/scala-swing_2.11-1.0.1.jar;libraries/org/scala-lang/scala-xml_2.11/1.0.2/scala-xml_2.11-1.0.2.jar;libraries/lzma/lzma/0.0.1/lzma-0.0.1.jar;libraries/net/sf/jopt-simple/jopt-simple/5.0.3/jopt-simple-5.0.3.jar;libraries/java3d/vecmath/1.5.2/vecmath-1.5.2.jar;libraries/net/sf/trove4j/trove4j/3.0.3/trove4j-3.0.3.jar;libraries/org/apache/maven/maven-artifact/3.5.3/maven-artifact-3.5.3.jar;libraries/com/mojang/patchy/1.1/patchy-1.1.jar;libraries/oshi-project/oshi-core/1.1/oshi-core-1.1.jar;libraries/net/java/dev/jna/jna/4.4.0/jna-4.4.0.jar;libraries/net/java/dev/jna/platform/3.4.0/platform-3.4.0.jar;libraries/com/ibm/icu/icu4j-core-mojang/51.2/icu4j-core-mojang-51.2.jar;libraries/net/sf/jopt-simple/jopt-simple/5.0.3/jopt-simple-5.0.3.jar;libraries/com/paulscode/codecjorbis/20101023/codecjorbis-20101023.jar;libraries/com/paulscode/codecwav/20101023/codecwav-20101023.jar;libraries/com/paulscode/libraryjavasound/20101123/libraryjavasound-20101123.jar;libraries/com/paulscode/librarylwjglopenal/20100824/librarylwjglopenal-20100824.jar;libraries/com/paulscode/soundsystem/20120107/soundsystem-20120107.jar;libraries/io/netty/netty-all/4.1.9.Final/netty-all-4.1.9.Final.jar;libraries/com/google/guava/guava/21.0/guava-21.0.jar;libraries/org/apache/commons/commons-lang3/3.5/commons-lang3-3.5.jar;libraries/commons-io/commons-io/2.5/commons-io-2.5.jar;libraries/commons-codec/commons-codec/1.10/commons-codec-1.10.jar;libraries/net/java/jinput/jinput/2.0.5/jinput-2.0.5.jar;libraries/net/java/jutils/jutils/1.0.0/jutils-1.0.0.jar;libraries/com/google/code/gson/gson/2.8.0/gson-2.8.0.jar;libraries/com/mojang/authlib/1.5.25/authlib-1.5.25.jar;libraries/com/mojang/realms/1.10.22/realms-1.10.22.jar;libraries/org/apache/commons/commons-compress/1.8.1/commons-compress-1.8.1.jar;libraries/org/apache/httpcomponents/httpclient/4.3.3/httpclient-4.3.3.jar;libraries/commons-logging/commons-logging/1.1.3/commons-logging-1.1.3.jar;libraries/org/apache/httpcomponents/httpcore/4.3.2/httpcore-4.3.2.jar;libraries/it/unimi/dsi/fastutil/7.1.0/fastutil-7.1.0.jar;libraries/org/apache/logging/log4j/log4j-api/2.8.1/log4j-api-2.8.1.jar;libraries/org/apache/logging/log4j/log4j-core/2.8.1/log4j-core-2.8.1.jar;libraries/org/lwjgl/lwjgl/lwjgl/2.9.4-nightly-20150209/lwjgl-2.9.4-nightly-20150209.jar;libraries/org/lwjgl/lwjgl/lwjgl_util/2.9.4-nightly-20150209/lwjgl_util-2.9.4-nightly-20150209.jar;libraries/com/mojang/text2speech/1.10.3/text2speech-1.10.3.jar;versions/1.12.2/1.12.2.jar";
            string entrypoint = "net.minecraft.launchwrapper.Launch";
            string minecraft_arguments = $"--username {session.Username} --version {targetVersion} --gameDir . --assetsDir assets --assetIndex 1.12 --uuid {session.UUID} --accessToken {session.AccessToken} --userType mojang --tweakClass net.minecraftforge.fml.common.launcher.FMLTweaker --versionType Forge --width 1280 --height 720";

            string arguments = $"{directives} {java_arguments} {libraries} {entrypoint} {minecraft_arguments}";

            Console.WriteLine(arguments);

            Process process = new Process();

            process.StartInfo.FileName = executable;
            process.StartInfo.WorkingDirectory =
                Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + ".minecraft";
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = true;

            process.Start();

            Console.ReadLine();
        }

        private string GetToken(string username, string password)
        {
            string clientToken = Guid.NewGuid().ToString().Replace("-", "");
            string request = "{ \"agent\" : { \"name\" : \"Minecraft\" , \"version\" : 1 }, \"username\" : \"" + username + "\", \"password\" : \"" + password + "\", \"clientToken\" : \"" + clientToken + "\"}";
            string response;
            HttpWebRequest http = WebRequest.CreateHttp("https://authserver.mojang.com/authenticate");
            http.ContentType = "application/json";
            http.Method = "POST";
            using (var req = new StreamWriter(http.GetRequestStream()))
            {
                req.Write(request);
                req.Flush();
            }
            using (Stream s = http.GetResponse().GetResponseStream())
            using (StreamReader reader = new StreamReader(s))
                response = reader.ReadToEnd();
            TokenResponse tResponse = JsonConvert.DeserializeObject<TokenResponse>(response);
            string token = tResponse.accessToken;
            Console.WriteLine($"Token: {token}");
            return token;
        }

        private void Synchronize(ModpackEntry[] entries)
        {
            // create a list of already existing config entries
            List<ModpackEntry> presentEntries = new List<ModpackEntry>();
            string baseDir = $".minecraft";

            int yProgress = Console.CursorTop > Console.WindowHeight ? Console.CursorTop : Console.WindowHeight;
            int yStatus = yProgress - 1;
            int width = Console.WindowWidth - 3;

            Console.SetCursorPosition(0, yStatus);
            string blank = "";
            for (int i = 0; i < width; i++)
                blank += " ";
            Console.Write(blank);
            Console.SetCursorPosition(0, yProgress);
            Console.Write(blank);

            int l = entries.Length;
            for (int i = 0; i < l; i++)
            {
                ModpackEntry entry = entries[i];
                string localPath = $"{baseDir}/{entry.ClientLocation}";
                if (File.Exists(localPath))
                {
                    // compare checksums
                    byte[] localBytes;
                    using (MD5 md5 = MD5.Create())
                    using (FileStream stream = File.OpenRead(localPath))
                        localBytes = md5.ComputeHash(stream);
                    string localChecksum = BitConverter.ToString(localBytes).Replace("-", "").ToLower();
                    string remoteChecksum = entry.Checksum;

                    if (localChecksum != remoteChecksum)
                    {
                        Console.SetCursorPosition(0, yStatus);
                        Console.Write(blank);
                        Console.SetCursorPosition(0, yStatus);
                        Console.Write($"{entry.Name} checksum mismatch, deleting");
                        File.Delete(localPath);
                    }
                    else
                    {
                        Console.SetCursorPosition(0, yStatus);
                        Console.Write(blank);
                        Console.SetCursorPosition(0, yStatus);
                        Console.Write($"{i + 1} out of {l}");

                        presentEntries.Add(entry);
                        Console.SetCursorPosition(0, yProgress);
                        Console.Write(blank);
                        Console.SetCursorPosition(0, yProgress);

                        string progress_line = "";
                        int inner_width = width - 2;
                        progress_line += "[";
                        float fraction = (float) i / l;
                        int filled_width = (int)(inner_width * fraction);
                        int empty_width = inner_width - filled_width;
                        for (int j = 0; j < filled_width; j++)
                            progress_line += "#";
                        for (int j = 0; j < empty_width; j++)
                            progress_line += " ";
                        progress_line += "]";
                        Console.Write(progress_line);
                    }
                    
                }
            }

            l = entries.Length;
            for(int i = 0; i < l; i++)
            {
                ModpackEntry entry = entries[i];
                if (presentEntries.Contains(entry))
                    continue;

                Console.SetCursorPosition(0, yStatus);
                Console.Write(blank);
                Console.SetCursorPosition(0, yStatus);
                Console.Write($"({i + 1} / {l}) - Pulling {entry.Name}");
                byte[] data = PullData(entry);
                if (data == null)
                {
                    Console.SetCursorPosition(0, yStatus);
                    Console.Write(blank);
                    Console.SetCursorPosition(0, yStatus);
                    Console.Write("Skipping.. ");
                    continue;
                }
                string loc = $".minecraft/{entry.ClientLocation}";
                Directory.CreateDirectory(Path.GetDirectoryName(loc));
                File.WriteAllBytes(loc, data);

                Console.SetCursorPosition(0, yProgress);
                Console.Write(blank);
                Console.SetCursorPosition(0, yProgress);

                string progress_line = "";
                int inner_width = width - 2;
                progress_line += "[";
                float fraction = (float)i / l;
                int filled_width = (int)(inner_width * fraction);
                int empty_width = inner_width - filled_width;
                for (int j = 0; j < filled_width; j++)
                    progress_line += "#";
                for (int j = 0; j < empty_width; j++)
                    progress_line += " ";
                progress_line += "]";
                Console.Write(progress_line);
            }
        }

        private byte[] PullData(ModpackEntry entry)
        {
            string url = $"http://{_host}:{_port}/pull {entry.Name}";
            try
            {
                using (HttpWebResponse response =
                    (HttpWebResponse) ((HttpWebRequest) WebRequest.Create(url)).GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (MemoryStream ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    return ms.ToArray();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Err: {e}");
                return null;
            }
            
        }

        private string PullChecksum(ModpackEntry entry)
        {
            string url = $"http://{_host}:{_port}/checksum {entry.ClientLocation}";
            using (HttpWebResponse response = (HttpWebResponse)((HttpWebRequest)WebRequest.Create(url)).GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
                return reader.ReadToEnd();
        }

    }

    class TokenResponse
    {
        public string accessToken;
    }
}
