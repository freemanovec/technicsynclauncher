﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace Synchronizer
{
    internal class ModpackInfo
    {

        public ModpackEntry[] Entries { get; }

        private readonly string _host;
        private readonly int _port;

        private ModpackInfo(
            string host,
            int port,
            ModpackEntry[] entries
        )
        {
            _host = host;
            _port = port;
            Entries = entries;
        }

        public static ModpackInfo Construct(string host, int port)
        {
            string url = $"http://{host}:{port}/info";
            string serialized;
            
            serialized = new WebClient().DownloadString(url);

            JsonObjectModpackDescription
                deserialized = JsonConvert.DeserializeObject<JsonObjectModpackDescription>(serialized);
            
            if(deserialized.files == null)
                throw new DataException();

            List<ModpackEntry> entries = new List<ModpackEntry>();
            foreach(KeyValuePair<string, JsonObjectFileEntry> entry in deserialized.files)
                entries.Add(new ModpackEntry()
                {
                    ClientLocation = entry.Value.loc_client,
                    ServerLocation = entry.Value.loc_server,
                    Name = entry.Key,
                    Checksum = entry.Value.checksum
                });
            
            return new ModpackInfo(
                host,
                port,
                entries.ToArray()
                );
        }
    }
    
    public class ModpackEntry
    {
        public string Name;
        public string ServerLocation;
        public string ClientLocation;
        public string Checksum;
    }

    public class JsonObjectModpackDescription
    {
        public Dictionary<string, JsonObjectFileEntry> files;
    }

    public class JsonObjectFileEntry
    {
        public string loc_server;
        public string loc_client;
        public string checksum;
    }
}
