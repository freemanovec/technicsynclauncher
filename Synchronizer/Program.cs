﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synchronizer
{
    class Program
    {
        static void Main(string[] args)
        {
            string host = "torsten.cz";
            int port = 25566; // will be 25566 on prod
            int ram = 8192;
            // pull and extract minecraft base
            Console.WriteLine("Creating instance");
            MinecraftInstance instance = new MinecraftInstance(host, port);
            Console.WriteLine("Instance created");
            // pull modpack info
            Console.WriteLine("Constructing modpack");
            ModpackInfo modpack = ModpackInfo.Construct(host, port);
            Console.WriteLine("Modpack constructed");
            ModpackEntry[] modpackEnties = modpack.Entries;
            // pull and install mods and configs
            Console.WriteLine("Synchronizing files");
            instance.SynchronizeFiles(modpackEnties);
            Console.WriteLine("Synchronizing done");
            // launch minecraft
            Console.WriteLine("Launching minecraft");
            instance.Launch(ram);
            // exit
            Console.WriteLine("All done");
        }
    }
}
